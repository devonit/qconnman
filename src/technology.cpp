/*
 * Copyright (C) 2012 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qconnman
 *
 * This file is part of the QConnman Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#include "qconnman_debug.h"

#include "interfaces/technology_interface.h"

#include "service.h"
#include "service_p.h"
#include "technology_p.h"
#include "technology.h"

Technology::Technology(const QDBusObjectPath &path, const QVariantMap &properties, QObject *parent)
    : ConnManObject(parent),
      d_ptr(new TechnologyPrivate(path))
{
    Q_D(Technology);
    d->technologyInterface =
        new NetConnmanTechnologyInterface("net.connman", path.path(), QDBusConnection::systemBus(), this);
    if (!d->technologyInterface->isValid()) {
        qConnmanDebug() << "unable to connect to service at path: " << path.path();
        return;
    }

    connect(d->technologyInterface, SIGNAL(PropertyChanged(QString,QDBusVariant)),
                             this, SLOT(propertyChanged(QString,QDBusVariant)));

    // set the initial properties
    foreach (QString property, properties.keys())
        propertyChanged(property, QDBusVariant(properties.value(property)));
}

Technology::~Technology()
{
}

QDBusObjectPath Technology::path() const
{
    Q_D(const Technology);
    return d->path;
}

bool Technology::isPowered() const
{
    Q_D(const Technology);
    return d->powered;
}

void Technology::setPowered(bool powered)
{
    Q_D(Technology);
    QDBusPendingReply<> reply =
        d->technologyInterface->SetProperty("Powered", QDBusVariant(powered));
    reply.waitForFinished();
    if (reply.isError()) {
        qConnmanDebug() << "error: " << reply.error().message();
    }
}

bool Technology::isConnected() const
{
    Q_D(const Technology);
    return d->connected;
}

QString Technology::name() const
{
    Q_D(const Technology);
    return d->name;
}

QString Technology::type() const
{
    Q_D(const Technology);
    return d->type;
}

bool Technology::tetheringAllowed() const
{
    Q_D(const Technology);
    return d->tethering;
}

void Technology::setTetheringAllowed(bool allowed)
{
    Q_D(Technology);
    QDBusPendingReply<> reply =
        d->technologyInterface->SetProperty("Tethering", QDBusVariant(allowed));
    reply.waitForFinished();
    if (reply.isError()) {
        qConnmanDebug() << "error: " << reply.error().message();
    }
}

QString Technology::tetheringIdentifier() const
{
    Q_D(const Technology);
    return d->tetheringIdentifier;
}

void Technology::setTetheringIdentifier(const QString &identifier)
{
    Q_D(Technology);
    QDBusPendingReply<> reply =
        d->technologyInterface->SetProperty("TetheringIdentifier", QDBusVariant(identifier));
    reply.waitForFinished();
    if (reply.isError()) {
        qConnmanDebug() << "error: " << reply.error().message();
    }
}

QString Technology::tetheringPassphrase() const
{
    Q_D(const Technology);
    return d->tetheringPassphrase;
}

void Technology::setTetheringPassphrase(const QString &passphrase)
{
    Q_D(Technology);
    QDBusPendingReply<> reply =
        d->technologyInterface->SetProperty("TetheringPassphrase", QDBusVariant(passphrase));
    reply.waitForFinished();
    if (reply.isError()) {
        qConnmanDebug() << "error: " << reply.error().message();
    }
}

void Technology::scan()
{
    Q_D(Technology);
    QDBusPendingReply<> reply = d->technologyInterface->Scan();
    QDBusPendingCallWatcher *watcher = new QDBusPendingCallWatcher(reply, this);
    connect(watcher, SIGNAL(finished(QDBusPendingCallWatcher*)),
               this, SIGNAL(scanCompleted()));
    connect(watcher, SIGNAL(finished(QDBusPendingCallWatcher*)), watcher, SLOT(deleteLater()));
}

void Technology::setPoweredInternal(bool powered)
{
    Q_D(Technology);
    d->powered = powered;
    Q_EMIT poweredChanged(powered);
}

void Technology::setConnectedInternal(bool connected)
{
    Q_D(Technology);
    d->connected = connected;
    Q_EMIT connectedChanged();
}

void Technology::setNameInternal(const QString &name)
{
    Q_D(Technology);
    d->name = name;
    Q_EMIT nameChanged();
}

void Technology::setTypeInternal(const QString &type)
{
    Q_D(Technology);
    d->type = type;
    Q_EMIT typeChanged();
}

void Technology::setTetheringAllowedInternal(bool allowed)
{
    Q_D(Technology);
    d->tethering = allowed;
    Q_EMIT tetheringAllowedChanged();
}

void Technology::setTetheringIdentifierInternal(const QString &identifier)
{
    Q_D(Technology);
    d->tetheringIdentifier = identifier;
    Q_EMIT tetheringIdentifierChanged();
}

void Technology::setTetheringPassphraseInternal(const QString &passphrase)
{
    Q_D(Technology);
    d->tetheringIdentifier = passphrase;
    Q_EMIT tetheringPassphraseChanged();
}
