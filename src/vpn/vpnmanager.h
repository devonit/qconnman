/*
 * Copyright (C) 2012 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qconnman
 *
 * This file is part of the QConnman Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#ifndef VPNMANAGER_H
#define VPNMANAGER_H

#include <QAbstractListModel>
#include <QDBusObjectPath>

class QDBusPendingCallWatcher;
class VpnAgent;
class VpnConnection;
class VpnManagerPrivate;
class VpnManager : public QAbstractListModel
{
    Q_OBJECT
public:
    VpnManager(QObject *parent = 0);
    virtual ~VpnManager();

public Q_SLOTS:
    QDBusObjectPath create(const QVariantMap &providerData);
    void remove(const QDBusObjectPath &path);
    void remove(VpnConnection *connection);

    QList<VpnConnection*> connections() const;
    VpnConnection *connection(const QDBusObjectPath &path) const;

    void registerAgent(VpnAgent *agent);
    void unregisterAgent(VpnAgent *agent);
    void unregisterAgent(const QDBusObjectPath &path);

public: // reimp QAbstractListModel
    virtual QVariant data(const QModelIndex &index, int role) const;
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;

private:
    Q_DISABLE_COPY(VpnManager)
    Q_DECLARE_PRIVATE(VpnManager)
    QScopedPointer<VpnManagerPrivate> d_ptr;

    Q_PRIVATE_SLOT(d_func(), void connmanVpnRegistered())
    Q_PRIVATE_SLOT(d_func(), void connmanVpnUnregistered())
    Q_PRIVATE_SLOT(d_func(), void getConnectionsResponse(QDBusPendingCallWatcher *call))

    Q_PRIVATE_SLOT(d_func(), void connectionAdded(const QDBusObjectPath &path, const QVariantMap &properties))
    Q_PRIVATE_SLOT(d_func(), void connectionRemoved(const QDBusObjectPath &path))

};

#endif
