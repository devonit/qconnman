/*
 * Copyright (C) 2012-2014 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qconnman
 *
 * This file is part of the QConnman Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#ifndef VPNCONNECTION_H
#define VPNCONNECTION_H

#include <QObject>
#include <QStringList>
#include <QDBusObjectPath>
#include <QDBusArgument>
#include <QScopedPointer>

#include "types.h"
#include "service.h"
#include "connmanobject.h"

class VpnConnection;
class VpnRouteDataPrivate;
class VpnRouteData : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int ProtocolFamily READ protocolFamily WRITE setProtocolFamily)
    Q_PROPERTY(QString Network READ network WRITE setNetwork)
    Q_PROPERTY(QString Netmask READ netmask WRITE setNetmask)
    Q_PROPERTY(QString Gateway READ gateway WRITE setGateway)
public:
    explicit VpnRouteData(VpnConnection *parent = 0);
    ~VpnRouteData();

    int protocolFamily() const;
    void setProtocolFamily(int protocolFamily);

    QString network() const;
    void setNetwork(const QString &network);

    QString netmask() const;
    void setNetmask(const QString &netmask);

    QString gateway() const;
    void setGateway(const QString &gateway);

private:
    Q_DISABLE_COPY(VpnRouteData)
    Q_DECLARE_PRIVATE(VpnRouteData)
    QScopedPointer<VpnRouteDataPrivate> d_ptr;

};

#if QT_VERSION <= 0x050000
Q_DECLARE_BUILTIN_METATYPE(VpnRouteData*, QObjectStar)
#else
Q_DECLARE_BUILTIN_METATYPE(VpnRouteDataStar, QMetaType::QObjectStar, VpnRouteData*)
#endif

Q_DECLARE_METATYPE(QList<VpnRouteData*>)

class VpnManager;
class VpnConnectionPrivate;
class VpnConnection : public ConnManObject
{
    Q_OBJECT
    Q_ENUMS(VpnConnectionState)
    Q_PROPERTY(QString State READ stateInternal WRITE setStateInternal NOTIFY dataChanged)
    Q_PROPERTY(QString Type READ type WRITE setTypeInternal NOTIFY dataChanged)
    Q_PROPERTY(QString Domain READ domain WRITE setDomainInternal NOTIFY dataChanged)
    Q_PROPERTY(QString Host READ host WRITE setHostInternal NOTIFY dataChanged)
    Q_PROPERTY(bool Immutable READ isImmutable WRITE setImmutableInternal NOTIFY dataChanged)
    Q_PROPERTY(int Index READ index WRITE setIndexInternal NOTIFY dataChanged)
    Q_PROPERTY(IPV4Data *IPv4 READ ipv4)
    Q_PROPERTY(IPV6Data *IPv6 READ ipv6)
    Q_PROPERTY(QStringList Nameservers READ nameservers WRITE setNameserversInternal NOTIFY dataChanged)
    Q_PROPERTY(QList<VpnRouteData*> UserRoutes READ userRoutes)
    Q_PROPERTY(QList<VpnRouteData*> ServerRoutes READ serverRoutes)

public:
    ~VpnConnection();

    enum VpnConnectionState {
        UndefinedState,
        IdleState,
        FailureState,
        ConfigurationState,
        ReadyState,
        DisconnectState
    };
    VpnConnectionState state() const;

    QDBusObjectPath objectPath() const;
    QString type() const;
    QString domain() const;
    QString host() const;
    bool isImmutable() const;
    int index() const;

    IPV4Data *ipv4() const;
    IPV6Data *ipv6() const;

    QStringList nameservers() const;

    QList<VpnRouteData*> userRoutes() const;
    QList<VpnRouteData*> serverRoutes() const;

Q_SIGNALS:
    void dataChanged();

public Q_SLOTS:
    void connect();
    void disconnect();

protected:
    VpnConnection(VpnConnectionPrivate *dd, QObject *parent = 0);
    Q_DECLARE_PRIVATE(VpnConnection)
    QScopedPointer<VpnConnectionPrivate> d_ptr;

private:
    VpnConnection(const QDBusObjectPath &path, const QVariantMap &properties, QObject *parent = 0);
    Q_DISABLE_COPY(VpnConnection)

    QString stateInternal() const;
    void setStateInternal(const QString &state);
    void setTypeInternal(const QString &type);
    void setDomainInternal(const QString &domain);
    void setHostInternal(const QString &host);
    void setImmutableInternal(bool immutable);
    void setIndexInternal(int index);
    void setNameserversInternal(const QStringList &nameservers);

    friend class VpnManagerPrivate;
};
Q_DECLARE_METATYPE(VpnConnection*)

class OpenConnectDataPrivate;
class OpenConnectVpnConnection;
class OpenConnectData : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString Cookie READ cookie WRITE setCookieInternal)
    Q_PROPERTY(QString ServerCert READ serverCert WRITE setServerCertInternal)
    Q_PROPERTY(QString CACert READ caCert WRITE setCaCertInternal)
    Q_PROPERTY(QString ClientCert READ clientCert WRITE setClientCertInternal)
    Q_PROPERTY(QString VPNHost READ vpnHost WRITE setVpnHostInternal)
    Q_PROPERTY(bool NoCertCheck READ noCertCheck WRITE setNoCertCheckInternal)
public:
    explicit OpenConnectData(OpenConnectVpnConnection *parent);
    ~OpenConnectData();

    QString cookie() const;
    QString serverCert() const;
    QString caCert() const;
    QString clientCert() const;
    QString vpnHost() const;
    bool noCertCheck() const;

private:
    void setCookieInternal(const QString &cookie);
    void setServerCertInternal(const QString &serverCert);
    void setCaCertInternal(const QString &caCert);
    void setClientCertInternal(const QString &clientCert);
    void setVpnHostInternal(const QString &vpnHost);
    void setNoCertCheckInternal(bool certCheck);

    Q_DISABLE_COPY(OpenConnectData)
    Q_DECLARE_PRIVATE(OpenConnectData)
    QScopedPointer<OpenConnectDataPrivate> d_ptr;

};
Q_DECLARE_METATYPE(OpenConnectData*)

class OpenConnectVpnConnectionPrivate;
class OpenConnectVpnConnection : public VpnConnection
{
    Q_OBJECT
    Q_PROPERTY(OpenConnectData *OpenConnect READ data CONSTANT)
public:

    OpenConnectData *data() const;

private:
    OpenConnectVpnConnection(const QDBusObjectPath &path, const QVariantMap &properties,
                             QObject *parent = 0);
    Q_DISABLE_COPY(OpenConnectVpnConnection)
    Q_DECLARE_PRIVATE(OpenConnectVpnConnection)

    friend class VpnManagerPrivate;
};
Q_DECLARE_METATYPE(OpenConnectVpnConnection*)

QDebug operator<<(QDebug, const VpnConnection *);
QDebug operator<<(QDebug, const QList<VpnRouteData*> &);
#endif
