/*
 * Copyright (C) 2012 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qconnman
 *
 * This file is part of the QConnman Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#ifndef CLOCK_H
#define CLOCK_H

#include <QStringList>
#include <QDateTime>

#include "connmanobject.h"

class QDBusPendingCallWatcher;
class ClockPrivate;
class Clock : public ConnManObject
{
    Q_OBJECT
    Q_ENUMS(UpdatePolicy)
    Q_PROPERTY(quint64 Time READ timeInternal WRITE setTimeInternal)
    Q_PROPERTY(QString TimeUpdates READ timeUpdatesInternal WRITE setTimeUpdatesInternal)
    Q_PROPERTY(QString Timezone READ timezone WRITE setTimezoneInternal)
    Q_PROPERTY(QString TimezoneUpdates READ timezoneUpdatesInternal WRITE setTimezoneUpdatesInternal)
    Q_PROPERTY(QStringList Timeservers READ timeservers WRITE setTimeserversInternal)

public:
    explicit Clock(QObject *parent = 0);
    ~Clock();

    enum UpdatePolicy { AutoPolicy, ManualPolicy };

    QDateTime time() const;
    void setTime(const QDateTime &time);

    UpdatePolicy timeUpdates() const;
    void setTimeUpdates(UpdatePolicy policy);

    QString timezone() const;
    void setTimezone(const QString &timezone);

    UpdatePolicy timezoneUpdates() const;
    void setTimezoneUpdates(UpdatePolicy policy);

    QStringList timeservers() const;
    void setTimeservers( const QStringList &servers );

Q_SIGNALS:
    void dataChanged();

private Q_SLOTS:
    void getPropertiesResponse(QDBusPendingCallWatcher *call);

private:
    static QHash<QString, UpdatePolicy> s_policyLookup;

    quint64 timeInternal() const;
    void setTimeInternal(quint64 time);
    QString timeUpdatesInternal() const;
    void setTimeUpdatesInternal(const QString &policy);
    void setTimezoneInternal(const QString &timezone);
    QString timezoneUpdatesInternal() const;
    void setTimezoneUpdatesInternal(const QString &policy);
    void setTimeserversInternal(const QStringList &servers );

    Q_DISABLE_COPY(Clock)
    Q_DECLARE_PRIVATE(Clock);
    QScopedPointer<ClockPrivate> d_ptr;
};

#endif // CLOCK_H
