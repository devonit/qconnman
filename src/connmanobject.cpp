/*
 * Copyright (C) 2012 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qconnman
 *
 * This file is part of the QConnman Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#include <QMetaProperty>
#include <QMetaObject>
#include <QDBusMetaType>
#include <QDebug>

#include "qconnman_debug.h"
#include "connmanobject.h"

ConnManObject::ConnManObject(QObject *parent)
    : QObject(parent)
{
}

ConnManObject::~ConnManObject()
{
}

void ConnManObject::propertyChanged(const QString &property, const QDBusVariant &value)
{
    QObject *object = this;
    QString modProperty(property);
    if (object->findChild<QObject*>(modProperty)) {
        object = object->findChild<QObject*>(modProperty);
    } else {
        if (modProperty.contains(".")) {
            QStringList parts = modProperty.split(".");
            modProperty = parts.takeLast(); // this is the property

            while (!parts.isEmpty()) {
                QString childName = parts.takeFirst();
                object = object->findChild<QObject*>(childName);
                if (!object) {
                    // might be an unsupported property name for Q_PROPERTY, pass it on
                    // unmolested
                    object = this;
                    modProperty = property;
                }
            }
        }
    }

    QVariant variant = value.variant();
    if (variant.userType() == qMetaTypeId<QDBusArgument>()) {
        // try to convert to a map and set those properties
        QVariantMap data = qdbus_cast<QVariantMap>(variant);
        foreach (QString key, data.keys()) {
            qConnmanDebug().nospace() << "propertyChanged(" << modProperty << "." << key << ", " << data.value(key) << ")";
            setObjectProperty(object, key, data.value(key));
        }
    } else {
        qConnmanDebug().nospace() << "propertyChanged(" << modProperty << ", " << variant << ")";
        setObjectProperty(object, modProperty, variant);
    }
}

void ConnManObject::setObjectProperty(QObject *object, const QString &property, const QVariant &value)
{
    const QMetaObject *mo = object->metaObject();
    int idx = mo->indexOfProperty(property.toLatin1());

    // some properties have dot syntax, which we can't support with Q_PROPERTY, so
    // check here if the combined version exists
    if (idx == -1 && property.contains(".")) {
        QString combined(property);
        combined.remove('.');
        idx = mo->indexOfProperty(combined.toLatin1());
    }

    if (idx == -1) {
        qConnmanDebug() << "\tinvalid property: " << property;
        return;
    }

    if (mo->property(idx).read(object) == value) {
        // avoid binding loops
        return;
    }

    if (!mo->property(idx).write(object, value)) {
        qConnmanDebug() << "\tcould not write property data: " << value;
        return;
    }

    qConnmanDebug() << "\twrote property(" << property << ") = " << value;
    const QMetaProperty mp = mo->property(idx);
    if (mp.hasNotifySignal())
        mp.notifySignal().invoke(object);

    Q_EMIT dataChanged();
}
