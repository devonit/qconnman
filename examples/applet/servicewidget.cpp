/*
 * Copyright (C) 2012 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qconnman
 *
 * This file is part of the QConnman Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#include <QPixmapCache>
#include <QTimeLine>
#include <QDebug>

#include "servicewidget.h"
#include "ui_servicewidget.h"

ServiceWidget::ServiceWidget(Service *service, QWidget *parent)
    : QAbstractButton(parent),
      ui(new Ui::ServiceWidget),
      m_service(service),
      m_loaderAnimation(0)
{
    ui->setupUi(this);
    m_loaderAnimation = new QTimeLine;
    m_loaderAnimation->setFrameRange(0, 21);
    m_loaderAnimation->setLoopCount(0);
    connect(m_loaderAnimation, SIGNAL(frameChanged(int)), this, SLOT(updateAnimation(int)));
    connect(this, SIGNAL(clicked()), SLOT(serviceClicked()));
    connect(service, SIGNAL(stateChanged()), this, SLOT(updateServiceData()));
    connect(service, SIGNAL(errorChanged()), this, SLOT(updateServiceData()));
    connect(service, SIGNAL(nameChanged()), this, SLOT(updateServiceData()));
    connect(service, SIGNAL(typeChanged()), this, SLOT(updateServiceData()));
    connect(service, SIGNAL(securityChanged()), this, SLOT(updateServiceData()));
    connect(service, SIGNAL(strengthChanged()), this, SLOT(updateServiceData()));
    connect(service, SIGNAL(isFavoriteChanged()), this, SLOT(updateServiceData()));
    connect(service, SIGNAL(isImmutableChanged()), this, SLOT(updateServiceData()));
    connect(service, SIGNAL(isAutoConnectChanged()), this, SLOT(updateServiceData()));
    connect(service, SIGNAL(isRoamingChanged()), this, SLOT(updateServiceData()));
    connect(service, SIGNAL(nameserversChanged()), this, SLOT(updateServiceData()));
    connect(service, SIGNAL(nameserversConfigurationChanged()), this, SLOT(updateServiceData()));
    connect(service, SIGNAL(timeserversChanged()), this, SLOT(updateServiceData()));
    connect(service, SIGNAL(timeserversConfigurationChanged()), this, SLOT(updateServiceData()));
    connect(service, SIGNAL(domainsChanged()), this, SLOT(updateServiceData()));
    connect(service, SIGNAL(domainsConfigurationChanged()), this, SLOT(updateServiceData()));
    updateServiceData();
}

ServiceWidget::~ServiceWidget()
{
    delete ui;
}

void ServiceWidget::paintEvent(QPaintEvent *e)
{
    Q_UNUSED(e)
}

void ServiceWidget::updateAnimation(int frame)
{
    QString imagePath =
        QString(":/images/brasero-disc-%1.png").arg(frame * 5, 2, 10, QLatin1Char('0'));

    QPixmap loaderImage;
    if (!QPixmapCache::find(imagePath, &loaderImage)) {
        loaderImage.load(imagePath);
        QPixmapCache::insert(imagePath, loaderImage);
    }

    ui->icon->setPixmap(loaderImage);
}

void ServiceWidget::updateServiceData()
{
    ui->label->setText(m_service->name());
    if (m_service->state() == Service::ReadyState|| m_service->state() == Service::OnlineState) {
        m_loaderAnimation->stop();

        QFont font = ui->label->font();
        font.setBold(true);
        ui->label->setFont(font);
    } else {
        if (m_service->state() == Service::FailureState || m_service->state() == Service::IdleState)
            m_loaderAnimation->stop();

        QFont font = ui->label->font();
        font.setBold(false);
        ui->label->setFont(font);
    }

    if (m_service->type() == QLatin1String("wifi")) {
        QString level;
        if (m_service->strength() <= 25)
            level = "0";
        else if (m_service->strength() <= 50)
            level = "25";
        else if (m_service->strength() <= 75)
            level = "50";
        else if (m_service->strength() <= 100)
            level = "75";
        else
            level = "100";

        if (m_service->security().contains(QLatin1String("none")) || m_service->security().isEmpty())
            ui->icon->setPixmap( QPixmap(QString(":/images/nm-signal-%1.png").arg(level)) );
        else
            ui->icon->setPixmap( QPixmap(QString(":/images/nm-signal-%1-secure.png").arg(level)) );
    } else if (m_service->type() == QLatin1String("ethernet")) {
        ui->icon->setPixmap(QPixmap(":/images/network-wired.png"));
    }
}

void ServiceWidget::serviceClicked()
{
    qDebug() << Q_FUNC_INFO;
    if (!m_loaderAnimation->state() == QTimeLine::Running)
        m_loaderAnimation->start();
    m_service->connect();
}

