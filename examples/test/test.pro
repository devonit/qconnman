DEPTH = ../..
include($${DEPTH}/qconnman.pri)

TEMPLATE = app
TARGET = tst_qconnman
DEPENDPATH += .
INCLUDEPATH += $${QCONNMAN_INCLUDEPATH}
LIBS += -L../../lib $${QCONNMAN_LIBS}

contains(QT_VERSION, ^5\\..*) {
    QT += widgets
}

SOURCES += tst_qconnman.cpp
